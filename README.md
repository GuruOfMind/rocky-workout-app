# ROCKY APP

This application is built for personal use. It's a gym plan organizer with large number of available physical exercises with theirs images.

# UI Mockup
 The Mockups have been designed using figma free design software, and The final result will be similar to the following screenshots:
 
1. Splash Screen

    ![Splash Screen](assets/mockups/2_intro_2.png)

2. Sign In with Google Screen

    ![Sign In screen](assets/mockups/5_signIn_with Google Account.png)

3. Main screen empty

    ![Main screen empty](assets/mockups/7_no program.png)

4. Plan Screen

    ![Plan screen](assets/mockups/12_week workouts.png)

5. Search exercise

    ![Search execrise](assets/mockups/14_search for exercise.png)

6. Exercise info

    ![Execrise info](assets/mockups/15_exercise info.png)
    

