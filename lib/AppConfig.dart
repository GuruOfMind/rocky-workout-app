
class AppConfig {
  static final AppConfig _instance = AppConfig._internal();
  factory AppConfig() => _instance;
  final startDate = DateTime(2019, 07, 08);
  final today = new DateTime.now();
  
  AppConfig._internal() {
    this.appName = 'ROCKY';
    this.daySeq = today.weekday;
    final difference = today.difference(startDate).inDays;
    this.sinceDays = difference;
  }

  // Methods, variables ...
  
  String appName;
  String description;
  double screenWidth;
  double screenHeight;
  double blockSizeVertical;
  double blockSizeHorizontal;
  int daySeq;
  int sinceDays;

  // final int daySeq = today.weekday;

  void setAppConfig({String appName, String description, double screenWidth, double screenHeight, double blockSizeVertical, double blockSizeHorizontal}) {
    this.appName = appName;
    this.description = description;
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;
    this.blockSizeVertical = blockSizeVertical;
    this.blockSizeHorizontal = blockSizeHorizontal;
  }
  
}