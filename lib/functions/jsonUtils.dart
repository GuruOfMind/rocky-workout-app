import 'dart:convert';
import 'package:rocky/data/program_PODO.dart';

List<Program> parseJosn(String response) {
      if(response==null){
        return [];
      }
      print(response);
      final parsed = json.decode(response.toString()).cast<Map<String, dynamic>>();
      return parsed.map<Program>((json) => new Program.fromJson(json)).toList();
  }