import 'package:rocky/AppConfig.dart';

AppConfig appConfig = new AppConfig();
int sinceDate = appConfig.sinceDays;

String survivedFor(){
  if(sinceDate <= 30)
  {
    return "Beginner_.png"; 
  }
  if(sinceDate <= 60)
  {
    return "Intermediate_.png";
  }
  if(sinceDate <= 90)
  {
    return "Advanced_.png";
  }
  if(sinceDate <= 150)
  {
    return "Master_.png";
  }
  if(sinceDate <= 210)
  {
    return "Expert_.png";
  }
  if(sinceDate <= 500)
  {
    return "Guru_.png";
  }

  return "Begineer_.png";
}