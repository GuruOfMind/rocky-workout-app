

import 'package:flutter/material.dart';

// const primaryColour = Color(0XFF06060D);
// const secondaryColour = Color(0XFF807471);
// const accentColour = Color(0XFFF8AE31);

// #06060D
// #807471
// #F8AE31

// #191937
// #141434
// #F4F5F6
// #FFDA79
// #42425B
// #56545D
// #BBBBC6
// #DFE0E5

const primaryColour = Color(0XFF141434);
const secondaryColour = Color(0XFFF4F5F6);
const accentColour = Color(0XFFFFDA79);

const inactiveColour = Color(0XFF42425B);

const textHeadingColour = Color(0XFF56545D);
const textTitleColour = Color(0XFFBBBBC6);
const textSmallColour = Color(0XFFDFE0E5);
 