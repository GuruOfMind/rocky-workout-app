import 'package:rocky/AppConfig.dart';

AppConfig appConfig = new AppConfig();

// final double paddingDistanceBig = appConfig.blockSizeHorizontal * 4;
// final double paddingDistanceSmall = appConfig.blockSizeHorizontal * 2;
final double paddingDistanceBig = 25.0;
final double paddingDistanceSmall = 15.0;

final double seperatorBoxHeight = appConfig.blockSizeVertical * 2;
final double seperatorBoxWidth = appConfig.blockSizeHorizontal * 2;