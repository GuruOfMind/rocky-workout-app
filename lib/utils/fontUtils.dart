

import 'package:rocky/AppConfig.dart';

AppConfig appConfig = new AppConfig();

final double fontBaseSize = appConfig.blockSizeVertical * 1.2;

final double fontHeadingSize = 32.0;
final double fontTitleSize = 24.0;
final double fontSubHeading = 18.0;
final double fontSmallSize = 14.0;
final String fontHeadingType = 'Big John';
final String fontNormalType = 'Slim Joe';