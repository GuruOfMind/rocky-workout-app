class Excercise {
  final int id;
  final String name;
  final String type;
  final String targetMuscle;
  final String secondaryMuscle;
  final String sets;
  final String reps;
  final String image;
  final String video;
  final List<int> alter;

  const Excercise(
      {this.id,
      this.name,
      this.type,
      this.targetMuscle,
      this.secondaryMuscle,
      this.sets,
      this.reps,
      this.image,
      this.video,
      this.alter});

  factory Excercise.fromJson(Map<String, dynamic> excerciseDetails) {
    return new Excercise(
      id: excerciseDetails["id"] as int,
      name: excerciseDetails["name"] as String,
      type: excerciseDetails["type"] as String,
      targetMuscle: excerciseDetails["targetMuscle"] as String,
      secondaryMuscle: excerciseDetails["secondaryMuscle"] as String,
      sets: excerciseDetails["sets"] as String,
      reps: excerciseDetails["reps"] as String,
      image: excerciseDetails["image"] as String,
      video: excerciseDetails["video"] as String,
      alter: excerciseDetails["alter"] as List<int>
    );
  }
}
