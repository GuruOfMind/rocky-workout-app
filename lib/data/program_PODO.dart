import 'package:rocky/data/excercise_PODO.dart';

class Program {

  final int id;
  final int seq;
  final String title;
  final String type;
  final List<String> images;
  final List<Excercise> excercises;

  Program({
    this.id,
    this.seq,
    this.title,
    this.type,
    this.images,
    this.excercises
  });

  factory Program.fromJson(Map<String, dynamic> dayDetails){
    return new Program(
      id: dayDetails['id'] as int,
      seq: dayDetails['seq'] as int,
      title: dayDetails['title'] as String,
      type: dayDetails['type'] as String,
      images: dayDetails['images'] as List,
      excercises: dayDetails['excercises'] as List,
    );
  }

}