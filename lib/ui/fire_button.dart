import 'package:flutter/material.dart';
import 'package:rocky/utils/fontUtils.dart';
import 'package:rocky/utils/colors.dart';

class FireButton extends StatelessWidget {
  const FireButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      color: accentColour,
      child: Text(
        'Just Do It',
        style: TextStyle(
            fontFamily: fontHeadingType,
            fontSize: fontTitleSize,
            color: inactiveColour),
      ),
    );
  }
}