import 'package:flutter/material.dart';
import 'package:rocky/AppConfig.dart';
import 'package:rocky/functions/timeTracking.dart';
import 'package:rocky/utils/distances.dart';
import 'package:rocky/utils/fontUtils.dart';

class AchievementCard extends StatelessWidget {
  
  const AchievementCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppConfig appConfig = new AppConfig();
    String badgesImage = survivedFor();
    return Container(
      height: 250.0,
      padding: EdgeInsets.only(
          left: paddingDistanceBig - 5.0,
          right: paddingDistanceBig,
          top: paddingDistanceBig),
      margin: EdgeInsets.only(
        left: paddingDistanceBig,
        right: paddingDistanceBig,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Started',
                  style: TextStyle(
                      fontSize: fontTitleSize,
                      fontFamily: fontNormalType,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      appConfig.sinceDays.toString(),
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: fontHeadingSize,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' Days',
                      style: TextStyle(
                          fontSize: fontHeadingSize,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            width: 15.0,
          ),
          Expanded(
            child: Container(
              child: Image.asset('assets/images/Badges/$badgesImage'),
            ),
          )
        ],
      ),
    );
  }
}
