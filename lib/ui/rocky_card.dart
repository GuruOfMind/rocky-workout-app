import 'package:flutter/material.dart';
import 'package:rocky/AppConfig.dart';
import 'package:rocky/data/program_PODO.dart';
import 'package:rocky/utils/colors.dart';
import 'package:rocky/utils/distances.dart';
import 'package:rocky/utils/fontUtils.dart';
import 'package:rocky/ui/muscl_holder.dart';

class RockyCard extends StatelessWidget {

  final List<Program> programList;
  const RockyCard({Key key, this.programList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AppConfig appConfig = new AppConfig();
    Program dayProgram;
    for(var day in programList)
    {
      if(day.seq == appConfig.daySeq)
      {
        dayProgram = day;
      }
    }

    return Container(
      height: 250.0,
      padding: EdgeInsets.only(
          left: paddingDistanceBig - 5.0,
          right: paddingDistanceBig,
          top: paddingDistanceBig + 15.0),
      margin: EdgeInsets.only(
        left: paddingDistanceBig,
        right: paddingDistanceBig,
      ),
      decoration: BoxDecoration(
        color: primaryColour,
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text(
              'my next workout'.toUpperCase(),
              style: TextStyle(
                fontFamily: fontNormalType,
                fontSize: fontSubHeading,
                fontWeight: FontWeight.bold,
                color: secondaryColour,
                letterSpacing: 1.0,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              dayProgram.title,
              style: TextStyle(
                fontFamily: fontHeadingType,
                fontSize: fontHeadingSize,
                fontWeight: FontWeight.bold,
                color: secondaryColour,
                letterSpacing: 1.0,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                MuscleHolder(
                  img: dayProgram.images[0],
                ),
                MuscleHolder(
                  img: dayProgram.images[1],
                ),
                MuscleHolder(
                  img: dayProgram.images[2],
                ),
                MuscleHolder(
                  img: dayProgram.images[3],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
