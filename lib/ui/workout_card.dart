import 'package:flutter/material.dart';
import 'package:rocky/data/excercise_PODO.dart';
import 'package:rocky/ui/muscl_holder.dart';
import 'package:rocky/utils/colors.dart';
import 'package:rocky/utils/distances.dart';
import 'package:rocky/utils/fontUtils.dart';

class WorkoutCard extends StatelessWidget {
  final Excercise excerciseDetails;
  WorkoutCard({Key key, this.excerciseDetails}) : super(key: key) {
    print(excerciseDetails.name);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.0,
      margin: EdgeInsets.symmetric(
        vertical: paddingDistanceSmall,
        horizontal: paddingDistanceBig,
      ),
      child: Stack(
        children: <Widget>[
          WorkoutPaper(
            excerciseDetail: this.excerciseDetails,
          ),
          WorkoutThumbnail(
            excerciseImage: excerciseDetails.image,
          )
        ],
      ),
    );
  }
}

class WorkoutPaper extends StatelessWidget {
  final Excercise excerciseDetail;
  const WorkoutPaper({
    Key key,
    this.excerciseDetail,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 124.0,
      margin: EdgeInsets.only(
        left: paddingDistanceBig,
      ),
      padding: EdgeInsets.only(
        left: paddingDistanceBig * 2,
        top: paddingDistanceBig * 1.5,
      ),
      decoration: BoxDecoration(
        color: secondaryColour,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            excerciseDetail.name.toString(),
            style: TextStyle(
              fontSize: fontSubHeading,
              fontFamily: fontHeadingType,
            ),
          ),
          Text(
            excerciseDetail.sets + " sets => " + excerciseDetail.reps,
            style: TextStyle(
              fontSize: fontSmallSize,
              fontFamily: 'Arial',
            ),
          )
        ],
      ),
    );
  }
}

class WorkoutThumbnail extends StatelessWidget {
  final String excerciseImage;
  const WorkoutThumbnail({
    Key key,
    this.excerciseImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: paddingDistanceSmall,
      ),
      alignment: FractionalOffset.centerLeft,
      child: MuscleHolder(
        img: excerciseImage,
      ),
    );
  }
}
