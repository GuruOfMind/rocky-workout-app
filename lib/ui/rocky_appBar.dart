import 'package:flutter/material.dart';
import 'package:rocky/utils/distances.dart';
import 'package:rocky/utils/fontUtils.dart';

class RockyBarApp extends StatelessWidget {
  final String title;
  final double initialHeight = 160.0;
  final Color titleColor;
  RockyBarApp({this.title, this.titleColor});

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    return Container(
      padding: EdgeInsets.only(top: statusBarHeight, left: paddingDistanceBig),
      height: statusBarHeight + initialHeight,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: fontHeadingSize,
                    fontFamily: fontHeadingType,
                    color: titleColor,
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Icon(
                  Icons.menu,
                  size: fontHeadingSize,
                  color: titleColor,
                ),
              ),
            ],
          ),
          Divider(
            color: titleColor,
            endIndent: paddingDistanceBig,
          )
        ],
      ),
    );
  }
}
