import 'package:flutter/material.dart';
import 'package:rocky/utils/colors.dart';

class MuscleHolder extends StatelessWidget {
  const MuscleHolder({
    Key key,
    this.img
  }) : super(key: key);

  final String img;
  @override
  Widget build(BuildContext context) {
    String imgPath = 'assets/images/Muscles/$img';
    return Container(
      width: 65.0,
      height: 65.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: inactiveColour,
      ),
      child: Image.asset(imgPath),

    );
  }
}
