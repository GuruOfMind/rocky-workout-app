import 'package:flutter/material.dart';
import 'package:rocky/AppConfig.dart';
// import 'package:rocky/screen/excerciseDetails.dart';
import 'package:rocky/screen/mainScreen.dart';
import 'package:rocky/screen/routes.dart';
// import 'package:rocky/screen/todayProgramScreen.dart';
// import 'package:rocky/screen/videoPlayerScreen.dart';
import 'package:rocky/utils/colors.dart';

void main() => runApp(MainClass());

class MainClass extends StatelessWidget {
  final AppConfig appConfig = new AppConfig();
  @override
  Widget build(BuildContext context) {
    appConfig.setAppConfig(
      appName: 'Rocky',
    );
    return MaterialApp(
      theme: ThemeData(
        primaryColor: primaryColour,
        accentColor: accentColour,
        primaryTextTheme: TextTheme(
          body1: TextStyle(
            color: textHeadingColour,
          ),
        ),
      ),
      title: appConfig.appName,
      home: SafeArea(
        child: MainScreen(),
      ),
      onGenerateRoute: (RouteSettings settings) {
        // ! This is duplicate root defination
        // '/': (context) => MainScreen(),
        // '/program-page': (context) => TodayProgramScreen(),
        // '/excercise-details': (context) => ExcerciseDetails(),
        // '/video-player': (context) => VideoPlayerScreen()
        return MaterialPageRoute(
          builder: (BuildContext context) => makeRoute(
                context: context,
                routeName: settings.name,
                arguments: settings.arguments,
              ),
          maintainState: true,
          fullscreenDialog: false,
        );
      },
    );
  }
}
