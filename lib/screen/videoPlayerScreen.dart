import 'package:flutter/material.dart';
import 'package:rocky/utils/colors.dart';
import 'package:rocky/utils/distances.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  final String videoName;
  VideoPlayerWidget({
    Key key,
    this.videoName,
  }) : super(key: key) {
    print("Video Name: " + videoName);
  }

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState(
        videoName: videoName,
      );
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  final String videoName;
  _VideoPlayerWidgetState({
    Key key,
    this.videoName,
  });

  VideoPlayerController _controller;
  VoidCallback listener;

  @override
  void initState() {
    try {
      super.initState();
      _controller = VideoPlayerController.asset('assets/videos/$videoName');
      _controller.addListener(() {
        setState(() {});
      });
      _controller.setVolume(0.0);
      _controller.setLooping(true);
      _controller.initialize().then((_) {
        setState(() {});
      });

    } catch (e) {
      print('Unexpected error: $e');
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool _visible = true;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (_controller.value.isPlaying) {
            _visible = true;
            _controller.pause();
          } else {
            _visible = false;
            _controller.play();
          }
        });
      },
      child: Stack(
        children: <Widget>[
          Container(
            child: _controller.value.initialized
                ? AspectRatio(
                    aspectRatio: _controller.value.aspectRatio,
                    child: VideoPlayer(_controller),
                  )
                : Container(),
          ),
          Center(
            child: AnimatedOpacity(
              opacity: _visible ? 1.0 : 0.0,
              duration: Duration(milliseconds: 300),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50.0),
                  color: primaryColour,
                ),
                padding: EdgeInsets.all(paddingDistanceBig),
                child: Icon(
                  _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                  color: accentColour,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
