import 'package:flutter/material.dart';
import 'package:rocky/data/program_PODO.dart';
import 'package:rocky/ui/rocky_appBar.dart';
import 'package:rocky/ui/workout_card.dart';
import 'package:rocky/utils/colors.dart';
import 'package:rocky/utils/distances.dart';
import 'package:rocky/utils/fontUtils.dart';

/*
  Vertical viewport was given unbounded height.
  Viewports expand in the scrolling direction to fill their container.In this case, a vertical
  viewport was given an unlimited amount of vertical space in which to expand. This situation
  typically happens when a scrollable widget is nested inside another scrollable widget.
  If this widget is always nested in a scrollable widget there is no need to use a viewport because
  there will always be enough vertical space for the children. In this case, consider using a Column
  instead. Otherwise, consider using the "shrinkWrap" property (or a ShrinkWrappingViewport) to size
  the height of the viewport to the sum of the heights of its children.

*/

class TodayProgramScreen extends StatefulWidget {
  final Program program;
  TodayProgramScreen({
    Key key,
    this.program,
  }) : super(key: key);

  _TodayProgramScreenState createState() =>
      _TodayProgramScreenState(program: program);
}

class _TodayProgramScreenState extends State<TodayProgramScreen> {
  final Program program;
  _TodayProgramScreenState({
    Key key,
    this.program,
  });
  // int _dismissibleKey = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: primaryColour,
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: RockyBarApp(
                title: program.title,
                titleColor: Colors.white,
              ),
            ),
            Expanded(
              flex: 3,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: 128.0 * (program.excercises.length + 1),
                    width: 5.0,
                    color: accentColour,
                    margin: EdgeInsets.fromLTRB(
                      paddingDistanceBig * 2,
                      paddingDistanceBig,
                      0,
                      0,
                    ),
                  ),
                  ListView.builder(
                    itemCount: program.excercises == null
                        ? 0
                        : program.excercises.length + 1,
                    itemBuilder: (BuildContext context, int index) {
                      final excercise = (index == program.excercises.length)
                          ? program.excercises[index - 1]
                          : program.excercises[index];
                      return (index == program.excercises.length)
                          ? SizedBox(
                              height: paddingDistanceBig * 2,
                            )
                          : Dismissible(
                              key: Key(excercise.name),
                              background: Container(
                                color: accentColour,
                              ),
                              onDismissed: (direction) {
                                setState(() {
                                  program.excercises.removeAt(index);
                                });
                                Scaffold.of(context).showSnackBar(
                                  SnackBar(
                                    backgroundColor: accentColour,
                                    content: Text(
                                      excercise.name + " is over",
                                      style: TextStyle(
                                        color: primaryColour,
                                      ),
                                    ),
                                  ),
                                );
                              },
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(
                                    context,
                                    '/excercise-details',
                                    arguments: program.excercises[index],
                                  );
                                },
                                child: WorkoutCard(
                                  excerciseDetails: program.excercises[index],
                                ),
                              ),
                            );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          elevation: 4.0,
          icon: Icon(
            Icons.arrow_back_ios,
            size: fontTitleSize,
          ),
          label: Text(
            'Go Back',
            style: TextStyle(
              fontSize: fontSubHeading,
              fontFamily: fontHeadingType,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }
}
