import 'package:flutter/material.dart';
import 'package:rocky/AppConfig.dart';
import 'package:rocky/data/excercise_PODO.dart';
import 'package:rocky/data/program_PODO.dart';
import 'package:rocky/ui/rocky_appBar.dart';
import 'package:rocky/ui/rocky_card.dart';
import 'package:rocky/utils/colors.dart';
import 'package:rocky/ui/fire_button.dart';
import 'package:rocky/ui/achievement_card.dart';
import 'package:rocky/utils/distances.dart';
import 'dart:convert';

class MainScreen extends StatefulWidget {
  const MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  List<Program> program = [];
  Future<List<Program>> _getDayInfo() async {
    var programData = await DefaultAssetBundle.of(context)
        .loadString('assets/data/program.json');
    var dataList = json.decode(programData);

    var res = dataList["Days"] as List;
    for (var day in res) {
      List<String> thumbnails = new List<String>.from(day['images']);

      List<Excercise> excercisesList = [];
      for (var exc in day['excercises']) {
        List<int> alterExc = new List<int>.from(exc["alter"]);
        Excercise item = Excercise(
          id: exc["id"],
          name: exc["name"],
          type: exc["type"],
          targetMuscle: exc["targetMuscle"],
          secondaryMuscle: exc["secondaryMuscle"],
          sets: exc["sets"],
          reps: exc["reps"],
          image: exc["image"],
          video: exc["video"],
          alter: alterExc,
        );
        excercisesList.add(item);
      }
      Program item = Program(
        id: day['id'],
        seq: day['seq'],
        title: day['title'],
        type: day['type'],
        images: thumbnails,
        excercises: excercisesList,
      );
      program.add(item);
    }
    return program;
  }

  @override
  Widget build(BuildContext context) {
    final AppConfig appConfig = new AppConfig();
    final screenH = MediaQuery.of(context).size.height;
    final blockSizeH = screenH / 100.0;
    final screenW = MediaQuery.of(context).size.width;
    final blockSizeV = screenW / 100.0;
    Program dayProgram;

    appConfig.setAppConfig(
      screenHeight: screenH,
      screenWidth: screenW,
      blockSizeHorizontal: blockSizeH,
      blockSizeVertical: blockSizeV,
    );
    

    return Scaffold(  
      backgroundColor: secondaryColour,
      body: Column(
        children: <Widget>[
          RockyBarApp(
            title: 'ROCKY',
            titleColor: primaryColour,
          ),
          Expanded(
            flex: 3,
            child: GestureDetector(
              child: FutureBuilder(
                future: _getDayInfo(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    print(snapshot.error);
                  }

                  return program.isNotEmpty
                      ? new RockyCard(programList: program)
                      : new Center(child: new CircularProgressIndicator());
                },
              ),
              onTap: () {
                for (var day in program) {
                  if (day.seq == appConfig.daySeq) {
                    dayProgram = day;
                  }
                }
                Navigator.pushNamed(context, '/program-page',
                    arguments: dayProgram);
              },
            ),
          ),
          SizedBox(
            height: seperatorBoxHeight,
          ),
          Expanded(
            flex: 2,
            child: AchievementCard(),
          ),
          SizedBox(
            height: seperatorBoxHeight,
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              child: FireButton(),
              onTap: () {
                for (var day in program) {
                  if (day.seq == appConfig.daySeq) {
                    dayProgram = day;
                  }
                }
                Navigator.pushNamed(context, '/program-page',
                    arguments: dayProgram);
              },
            ),
          ),
        ],
      ),
    );
  }
}
