import 'package:flutter/material.dart';
import 'package:rocky/data/excercise_PODO.dart';
import 'package:rocky/data/program_PODO.dart';
import 'package:rocky/screen/excerciseDetails.dart';
import 'package:rocky/screen/todayProgramScreen.dart';
import 'package:rocky/screen/videoPlayerScreen.dart';

Widget makeRoute({
  @required BuildContext context,
  @required String routeName,
  Object arguments,
}) {
  final Widget child =
      _buildRoute(context: context, routeName: routeName, arguments: arguments);
  return child;
}

Widget _buildRoute({
  @required BuildContext context,
  @required String routeName,
  Object arguments,
}) {
  switch (routeName) {
    case '/program-page':
      {
        Program program_ = arguments as Program;
        return TodayProgramScreen(program: program_);
      }
    case '/excercise-details':
      // Article article = arguments as Article;
      // return ArticleView(article: article);
      {
        Excercise excercise_ = arguments as Excercise;
        return ExcerciseDetails(excercise: excercise_);
      }
    case '/video-player':
      return VideoPlayerWidget();
    default:
      throw 'Route $routeName is not defined';
  }
}
