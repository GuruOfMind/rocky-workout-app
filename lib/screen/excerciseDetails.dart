import 'package:flutter/material.dart';
import 'package:rocky/data/excercise_PODO.dart';
import 'package:rocky/screen/videoPlayerScreen.dart';
import 'package:rocky/ui/rocky_appBar.dart';
import 'package:rocky/utils/colors.dart';
import 'package:rocky/utils/distances.dart';
import 'package:rocky/utils/fontUtils.dart';

class ExcerciseDetails extends StatefulWidget {
  final Excercise excercise;
  ExcerciseDetails({
    Key key,
    this.excercise,
  }) : super(key: key);

  _ExcerciseDetailsState createState() => _ExcerciseDetailsState(excercise: excercise);
}

class _ExcerciseDetailsState extends State<ExcerciseDetails> {
  final Excercise excercise;
  _ExcerciseDetailsState({
    Key key,
    this.excercise,
  });
  @override
  Widget build(BuildContext context) {
    // double c_width = MediaQuery.of(context).size.width * 0.8;
    return SafeArea(
      child: Scaffold(
        backgroundColor: primaryColour,
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              RockyBarApp(
                title: excercise.name,
                titleColor: Colors.white,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: paddingDistanceBig),
                height: 203.0,
                color: accentColour,
                child: VideoPlayerWidget(videoName: excercise.video),
              ),
              SizedBox(
                height: paddingDistanceSmall * 2,
              ),
              excerciseInfo(excercise),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          elevation: 4.0,
          icon: Icon(
            Icons.arrow_back_ios,
            size: fontTitleSize,
            color: inactiveColour,
          ),
          label: Text(
            'Go Back',
            style: TextStyle(
              fontSize: fontSubHeading,
              fontFamily: fontHeadingType,
              color: inactiveColour,
            ),
          ),
          onPressed: () {
            // Navigator.pushNamed(context, '/video-player');
            Navigator.pop(context);
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }

  Container excerciseInfo(Excercise excercise) {
    return Container(
      margin: EdgeInsets.only(
          left: paddingDistanceBig,
          right: paddingDistanceBig,
          bottom: paddingDistanceBig * 2.8),
      padding: EdgeInsets.only(left: paddingDistanceBig),
      height: 300.0,
      color: secondaryColour,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                width: 120.0,
                child: Text(
                  'Sets',
                  style: TextStyle(
                    fontFamily: fontHeadingType,
                    fontSize: fontTitleSize,
                    color: primaryColour,
                  ),
                ),
              ),
              Text(
                excercise.sets + " => " + excercise.reps,
                style: TextStyle(
                  fontFamily: fontHeadingType,
                  fontSize: fontSubHeading,
                  color: inactiveColour,
                ),
              ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 120.0,
                child: Text(
                  'Target',
                  style: TextStyle(
                    fontFamily: fontHeadingType,
                    fontSize: fontTitleSize,
                    color: primaryColour,
                  ),
                ),
              ),
              Container(
                height: 100.0,
                width: 200.0,
                child: Center(
                  child: Text(
                    excercise.targetMuscle + " - " + excercise.secondaryMuscle,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: fontHeadingType,
                      fontSize: fontSmallSize,
                      color: inactiveColour,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            // mainAxisAlignment: ,
            children: <Widget>[
              Container(
                width: 120.0,
                child: Text(
                  'Alter',
                  style: TextStyle(
                    fontFamily: fontHeadingType,
                    fontSize: fontTitleSize,
                    color: primaryColour,
                  ),
                ),
              ),
              Container(
                height: 100.0,
                width: 200.0,
                child: Center(
                    child: ListView(
                  children: <Widget>[
                    InkWell(
                      child: Text(
                        'Barbell Full Squat',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: fontHeadingType,
                          fontSize: fontSmallSize,
                          color: inactiveColour,
                        ),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/excercise-details');
                      },
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    InkWell(
                      child: Text(
                        'Barbell Hack Squat',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: fontHeadingType,
                          fontSize: fontSmallSize,
                          color: inactiveColour,
                        ),
                      ),
                      onTap: () {},
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                  ],
                )),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
